#!/bin/bash


NODES=${1:-3}
NAME=openfaas
OSTYPE=RedHat_64
MEMORY=512
## Change to "off" if your hardware does not support Intel VT-x or AMD-V
HVIRTEX=on
## Use "bridged" instead of "natnetwork" if you wish to expose the cluster to your LAN
## Currently bridged is the only one supported, natnetwork requires some extra settings
NIC=bridged # natnetwork
## Change it for the device used in your system, eth0, eno1, wlo1, etc..
NICDEV="eno1"
DISKSIZE=8000  # MB

## Do not change unless you know what you are doing.
DISKFORMAT=VDI
DISKVARIANT=Standard  ## ,Fixed,Split2G
VMDIR="$(dirname "${BASH_SOURCE[0]}")/../VMs"
if [ ! -d "${VMDIR}" ]; then
  mkdir "${VMDIR}"
fi

for vmnode in $(seq 1 ${NODES}); do
  NODENAME="${NAME}"0"${vmnode}"
  echo "${NODENAME}"
  DISKNAME="${NODENAME}.${DISKFORMAT}"
  SATACTL="SATA Controller"
  IDECTL="IDE Controller"

  ## Create and register the VM on VirtualBox
  VBoxManage createvm --name "${NODENAME}" \
    --basefolder "${VMDIR}/${NAME}" \
    --groups "/${NAME}" \
    --ostype "${OSTYPE}" \
    --register

  ## Configure RAM, Networking and HW virtualization support
  VBoxManage modifyvm "${NODENAME}" \
    --memory "${MEMORY}" \
    --hwvirtex "${HVIRTEX}" \
    --nic1 "${NIC}" \
    --bridgeadapter1 "${NICDEV}" \
    --vram 16 \
    --usb off \
    --audio none \
    --vrde on

  ## Configure storage
  VBoxManage storagectl "${NODENAME}" --name "${SATACTL}" --add sata --controller IntelAhci
  VBoxManage storagectl "${NODENAME}" --name "${IDECTL}" --add ide --controller PIIX4
  #VBoxManage createmedium disk --filename "${VMDIR}/${DISKNAME}" \
    #--size "${DISKSIZE}" \
    #--variant "Standard"
  ## VirtualBox uses same UUID when HDDs are copies, force change of UUID
  VBoxManage internalcommands sethduuid "${VMDIR}/../centos_${NODENAME}.vdi"
  VBoxManage storageattach "${NODENAME}" --storagectl "${SATACTL}" \
    --port 0 --device 0 --type hdd \
    --medium "${VMDIR}/../centos_${NODENAME}.vdi"  # ${DISKNAME}"node
  VBoxManage storageattach "${NODENAME}" --storagectl "${IDECTL}" \
    --port 1 --device 0 --type dvddrive \
    --medium "${VMDIR}/../init.iso"
  sleep 5
done
