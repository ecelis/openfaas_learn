# Server less functions with OpenFaaS

## Technologies

* VirtualBox - Hypervisor
* CentOS atomic host - Node OS
* cloud-init - Early initialization of cloud
* Docker - Isolation environement
* Ansible - Provisioning and automation
* Vagrant - ???
* ??? - Testing

## Cluster setup

### TODO title

#### Prep the cloud-init source ISO

    cat >>user-data<<EOF
    #cloud-config
    password: atomic
    ssh_pwdauth: True
    chpasswd: { expire: False }
    genisoimage -output init.iso -volid cidata -joliet \
        -rock user-data meta-data
    EOF

    cat >>meta-data<<EOF
    instance-id: of-host001
    local-hostname: of01.example.org
    EOF

    genisoimage -output init.iso -volid cidata -joliet \
        -rock user-data meta-data


#### Create the Atomic host virtual machine

    curl -LSO http://cloud.centos.org/centos/7/atomic/images/CentOS-Atomic-Host-GenericCloud.qcow2.gz
    xz -d CentOS-Atomic-Host-GenericCloud.qcow2.xz
    qemu-img convert -f qcow2 \
        CentOS-Atomic-Host-GenericCloud.qcow2 \
        -O vdi centos.vdi
    ./scripts/createvm.sh


#### Add and configure storage for Docker

    sudo fdisk -l
    /etc/sysconfig/docker-storage-setup

    DEVS="/dev/vdb"
    ROOT_SIZE=4G

    sudo docker-storage-setup
    sudo docker info
    sudo xfs_growfs /
    df -Th


#### Docker

    docker swarm init --advertise-addr x.x.x.x


#### Optional

     sudo docker service create \
         --name viz \
         --publish 82:8080/tcp \
         --constraint node.role==manager \
         --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
          dockersamples/visualizer

### Vagrant

TODO


    vagrant init centos/atomic-host



## References

* [Project Atomic Quick Start Guide](http://www.projectatomic.io/docs/quickstart/)
* [Get Started with CentOS Atomic Host](https://wiki.centos.org/SpecialInterestGroup/Atomic/Download)
